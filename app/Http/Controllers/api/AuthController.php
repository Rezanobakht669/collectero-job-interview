<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    /**
     * @group Authentication
     * User Login
     *
     * Authenticates a user by their provided credentials and returns an access token upon successful login.
     * If the user is already registered, it verifies the username and password combination and logs in the user.
     * If the user is not registered, it creates a new user with the provided credentials, logs them in, and returns an access token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     *
     * @response 200 {
     *     "status": "success",
     *     "data": {
     *         "user": {
     *             "id": 1,
     *             "username": "example_user"
     *             // additional user details
     *         },
     *         "token": "xxxxxxxxxxxxxxxxxxxx"
     *     },
     *     "message": "Logged In Successful"
     * }
     *
     * @response 401 {
     *     "status": "failed",
     *     "data": null,
     *     "message": "Invalid username or password"
     * }
     *
     * @response 201 {
     *     "status": "success",
     *     "data": {
     *         "user": {
     *             "id": 2,
     *             "username": "new_user"
     *             // additional user details
     *         },
     *         "token": "yyyyyyyyyyyyyyyyyyyy"
     *     },
     *     "message": "User Successfully Created and Logged In"
     * }
     *
     * @response 400 {
     *     "status": "failed",
     *     "data": null,
     *     "message": "Something went wrong while creating the user"
     * }
     */
    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required|string|min:3|max:20',
            'password' => 'required|string|min:8'
        ]);

        $user = User::where('username', $request->username)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                Auth::login($user);
                $accessToken = Auth::user()->createToken('authToken')->accessToken;
                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'user' => $user,
                        'token' => $accessToken
                    ],
                    'message' => 'Logged In Successful'
                ]);
            } else {
                return response()->json([
                    'status' => 'failed',
                    'data' => null,
                    'message' => 'password incorrect'
                ], 401);
            }
        }else   {
            DB::beginTransaction();
            try {
                $newUser = new User();
                $newUser->username = $request->username;
                $newUser->password = Hash::make($request->password);
                $newUser->save();
                DB::commit();
            }catch (QueryException $exception){
                DB::rollBack();
                return response()->json([
                   'status' => 'failed',
                   'data' => null,
                   'message' => 'somethings wrong'
                ],400);
            }
            Auth::login($newUser);
            $accessToken = Auth::user()->createToken('authToken')->accessToken;
            return response()->json([
                'status' => 'success',
                'data' => [
                    'user' => $newUser,
                    'token' => $accessToken
                ],
                'message' => 'User Successfully Created and Logged In'
            ],201);

        }
    }
}
