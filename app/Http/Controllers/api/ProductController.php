<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @group Products
     * Get Product List
     *
     * Retrieves a list of products with their corresponding details.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @response 200 {
     *     "status": "success",
     *     "data": [
     *         {
     *             "id": 1,
     *             "title": "iPhone 13 Pro",
     *             "image": "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-13-pro.jpg"
     *         },
     *         {
     *             "id": 2,
     *             "title": "iPhone 13",
     *             "image": "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-13.jpg"
     *         },
     *         // additional product details
     *     ],
     *     "message": null
     * }
     */
    public function index()
    {

        return response()->json([
            'status' => 'success',
             'data' =>   [
                 [
                    "id" => 1,
                    "title" => "iPhone 13 Pro",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-13-pro.jpg"
                ],
                [
                    "id" => 2,
                    "title" => "iPhone 13",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-13.jpg"
                ],
                [
                    "id" => 3,
                    "title" => "iPhone 12 Pro",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-12-pro.jpg"
                ],
                [
                    "id" => 4,
                    "title" => "iPhone 12",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-12.jpg"
                ],
                [
                    "id" => 5,
                    "title" => "iPhone SE (2020)",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-se-2020.jpg"
                ],
                [
                    "id" => 6,
                    "title" => "iPhone 11 Pro",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-11-pro-new.jpg"
                ],
                [
                    "id" => 7,
                    "title" => "iPhone 11",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-11-new.jpg"
                ],
                [
                    "id" => 8,
                    "title" => "iPhone XS",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-xs-new.jpg"
                ],
                [
                    "id" => 9,
                    "title" => "iPhone XR",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-xr-new.jpg"
                ],
                [
                    "id" => 10,
                    "title" => "iPhone X",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-x-new.jpg"
                ]
            ],
            'message' => null
            ]);

    }


    /**
     * @group Products
     * Get Product Details
     *
     * Retrieves the details of a specific product identified by its ID.
     *
     * @param string $id The ID of the product.
     * @return \Illuminate\Http\JsonResponse
     *
     * @response 200 {
     *     "status": "success",
     *     "data": {
     *         "id": 1,
     *         "title": "iPhone 13 Pro",
     *         "image": "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-13-pro.jpg"
     *     },
     *     "message": null
     * }
     *
     * @response 400 {
     *     "status": "failed",
     *     "data": null,
     *     "message": "Wrong Id"
     * }
     */
    public function show(string $id)
    {
        $result = null;

        switch ($id) {
            case "1":
                $result = [
                    "id" => 1,
                    "title" => "iPhone 13 Pro",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-13-pro.jpg"
                ];
                break;
            case "2":
                $result = [
                    "id" => 2,
                    "title" => "iPhone 13",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-13.jpg"
                ];
                break;
            case "3":
                $result = [
                    "id" => 3,
                    "title" => "iPhone 12 Pro",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-12-pro.jpg"
                ];
                break;
            case "4":
                $result = [
                    "id" => 4,
                    "title" => "iPhone 12",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-12.jpg"
                ];
                break;
            case "5":
                $result = [
                    "id" => 5,
                    "title" => "iPhone SE (2020)",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-se-2020.jpg"
                ];
                break;
            case "6":
                $result = [
                    "id" => 6,
                    "title" => "iPhone 11 Pro",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-11-pro-new.jpg"
                ];
                break;
            case "7":
                $result = [
                    "id" => 7,
                    "title" => "iPhone 11",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-11-new.jpg"
                ];
                break;
            case "8":
                $result = [
                    "id" => 8,
                    "title" => "iPhone XS",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-xs-new.jpg"
                ];
                break;
            case "9":
                $result = [
                    "id" => 9,
                    "title" => "iPhone XR",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-xr-new.jpg"
                ];
                break;
            case "10":
                $result = [
                    "id" => 10,
                    "title" => "iPhone X",
                    "image" => "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-x-new.jpg"
                ];
                break;
            default: return response()->json([
               'status' => 'failed',
                'data' => null,
                'message' => 'Wrong Id'
            ],400);
        }

        return response()->json([
            'status' => 'success',
            'data' => $result,
            'message' => null
        ]);
    }


}
