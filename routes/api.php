<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\AuthController;
use \App\Http\Controllers\api\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('auth/login',[AuthController::class,'login']);

Route::middleware(['auth:api'])->group(function (){
    Route::prefix('products')->group(function (){
        Route::get('list',[ProductController::class,'index']);
        Route::get('show/{id}',[ProductController::class,'show']);
    });
});
